FROM php:7-apache

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions  \
    && install-php-extensions pdo_pgsql pdo_mysql intl 

RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls && \
    mv composer.phar /usr/local/bin/composer

RUN apt-get update \
    && apt-get install -y libzip-dev zip

COPY ./symfony-120922-demo1 /var/www/

COPY ./apache.conf /etc/apache2/sites-available/000-default.conf

RUN cd /var/www && composer install

WORKDIR /var/www/

EXPOSE 80
